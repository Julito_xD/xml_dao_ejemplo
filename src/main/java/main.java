import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;

import java.io.File;

public class main {
    public static void main(String[] args) {
        JAXBContext jaxbContext;
        File file = new File("libreriaDao.xml");
        XmlLibroImpl libreria;

        //CREANDO EL ENTORNO
        try {
            jaxbContext = JAXBContext.newInstance(XmlLibroImpl.class);

            //CÓDIGO PARA LEER XML
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();           //Crea el lector de XML.
            libreria = (XmlLibroImpl) jaxbUnmarshaller.unmarshal(file);                 //Lee el XML y lo almacena en una variable.
            System.out.println("BBDD:" + libreria + " correctamente cargardo");

            //CÓDIGO PARA ESCRIBIR XML
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();                 //Crea el escritor de XML.
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE); //Ordena la escritura en formato XML.
            //jaxbMarshaller.marshal(libreria, file);                                   //Imprime el XML sobre el fichero.
            jaxbMarshaller.marshal(libreria, System.out);                               //Imprime el XML sobre la consola.

            //ESCRIBA AQUÍ, LAS NUEVAS FUNCIONALIDADES:
            Libro l1 = new Libro(7,"Los Gallinazos sin plumas");
            //libreria.updateLibro(l1);
            //libreria.deleteLibro(7);
            //libreria.getLibro(4);

            //jaxbMarshaller.marshal(libreria, file);                                     //Imprime el XML sobre el fichero.
            //jaxbMarshaller.marshal(libreria, System.out);                               //Imprime el XML sobre la consola.

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
