import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

//EL TRANSFER CORRRESPONDE A LA CLASE NORMAL
@XmlRootElement(name="libro")
@XmlType(propOrder = {"id","name"})
public class Libro {
    private int id;
    private String name;

    public Libro() {
    }

    public Libro(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @XmlAttribute(name="id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @XmlElement(name="name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}