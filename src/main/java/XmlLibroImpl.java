import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementWrapper;
import jakarta.xml.bind.annotation.XmlRootElement;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name="libros")
public class XmlLibroImpl implements LibroDAO{
    //------------------------------- ATRIBUTOS -------------------------------//
    @XmlElementWrapper
    List<Libro> libros;

    //------------------------------ CONSTRUCTOR ------------------------------//
    public XmlLibroImpl(){
        libros = new ArrayList<Libro>();
    }

    //--------------------------- IMPLEMENTACIONES ---------------------------//
    JAXBContext jaxbContext;
    File file = new File("libreriaDao.xml");
    XmlLibroImpl libreria;

    @Override
    public void updateLibro(Libro libro) {
       libros.add(libro);

         try{
            //CREACIÓN DEL ENTORNO
            jaxbContext = JAXBContext.newInstance(XmlLibroImpl.class);

            //CÓDIGO PARA ESCRIBIR XML
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();                 //Crea el escritor de XML.
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE); //Ordena la escritura en formato XML.
            jaxbMarshaller.marshal(this, file);                                      //Imprime el XML sobre el fichero.
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        System.out.println("Libro: "+ libro.getName() + ", id " + libro.getId() + ", updated in the database");
    }
    //------------------------------------------------------------------------//
    @Override
    public void deleteLibro(int id) {
        libros.remove(libros.get(id));

        try{
            //CREACIÓN DEL ENTORNO
            jaxbContext = JAXBContext.newInstance(XmlLibroImpl.class);

            //CÓDIGO PARA ESCRIBIR XML
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();                 //Crea el escritor de XML.
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE); //Ordena la escritura en formato XML.
            jaxbMarshaller.marshal(this, file);                                      //Imprime el XML sobre el fichero.
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        System.out.println("Book deleted from database");
    }

    //------------------------------------------------------------------------//
    @Override
    public void getLibro(int id) {
        System.out.println("Libro: " + libros.get(id).getId() + ", " + libros.get(id).getName());
    }

    //------------------------------------------------------------------------//
    @Override
    @XmlElement(name="libro")
    public void getAllLibros() {
        for(Libro l: libros){
            System.out.println("Libro: " + l.getId() + ", " +l.getName());
        }
    }
}
