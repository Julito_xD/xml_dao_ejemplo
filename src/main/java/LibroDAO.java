public interface LibroDAO {
    public void updateLibro (Libro libro);
    public void deleteLibro (int id);
    public void getLibro(int id);
    public void getAllLibros();
}
